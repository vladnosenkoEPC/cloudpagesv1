let googleCalls = 0;
const maxGoogleCalls = 20;
const defaultPlanId = "spt_1_op_1500_annualy";

document.addEventListener("DOMContentLoaded", () => {
  let cbInstance = Chargebee.init({
    site: "phoenixfr-test",
    publishableKey: "test_fMQa6UklFaz28a61FFmnSX0D0Q7Ph7rS",
  });

  var options = {
    fonts: ["https://fonts.googleapis.com/css?family=Roboto:300,500,600"],
    // add classes for different states
    classes: {
      focus: "focus",
      invalid: "invalid",
      empty: "empty",
      complete: "complete",
    },

    // add placeholders
    placeholder: {
      number: "0000 0000 0000 0000",
    },

    // Set locale
    locale: "en",

    style: {
      // Styles for default state
      base: {
        color: "#333",
        fontWeight: "500",
        fontFamily: "Arial, sans-serif",
        fontSize: "16px",
        fontSmoothing: "antialiased",

        ":focus": {
          // color: '#424770',
        },

        "::placeholder": {
          color: "#abacbe",
        },

        ":focus::placeholder": {
          // color: '#7b808c',
        },
      },

      // Styles for invalid state
      invalid: {
        color: "#E94745",

        ":focus": {
          color: "#e44d5f",
        },
        "::placeholder": {
          color: "#FFCCA5",
        },
      },
    },
  };

  cbInstance.load("components").then(() => {
    var cardComponent = cbInstance.createComponent("card", options);
    // Create card fields
    cardComponent.createField("number").at("#card-number");
    cardComponent.createField("expiry").at("#card-expiry");
    cardComponent.createField("cvv").at("#card-cvc");

    // Mount card component
    cardComponent.mount();

    formSubmitting(cardComponent);
  });

  setZipValues();
  setHolderName();
});

const formSubmitting = (cardComponent) => {
  const formNode = document.querySelector("#subscription-form");

  formNode.addEventListener("submit", async (e) => {
    e.preventDefault();

    const isFormValid = formValidation(formNode);

    const formData = new FormData(formNode);
    if (!isFormValid) return;

    let additionData = {
      firstName: formData.get("firstName"),
      lastName: formData.get("lastName"),
      billingAddress: {
        billingAddr1: formData.get("line1"),
        billingAddr2: formData.get("line2"),
        billingCity: formData.get("city"),
        billingState: formData.get("state"),
        billingStateCode: "",
        billingZip: formData.get("zip"),
        billingCountry: formData.get("country"),
      },
    };

    const token = await handleTokenize(cardComponent, additionData);

    if (token) {
      const formattedData = {
        email: formData.get("email"),
        firstName: formData.get("firstName"),
        lastName: formData.get("lastName"),
        billingToken: token,
        planId: defaultPlanId,
        billingAddress: {
          holderName: formData.get("holderName"),
          phone: formData.get("phone"),
          city: formData.get("city"),
          state: formData.get("state"),
          country: formData.get("country"),
          zip: formData.get("zip"),
          line1: formData.get("line1"),
          line2: formData.get("line2"),
          line3: "",
        },
      };
      postSubscription(formattedData);
    } else {
      console.log("empty resp");
    }
  });
};

const handleTokenize = async (cardComponent, additionalData) => {
  const data = await cardComponent.tokenize().catch((error) => {
    console.log("tokenize error", error);
  });

  return data.token;
};

const formValidation = (formNode) => {
  let inputArray = [].slice.call(formNode.getElementsByTagName("input"));

  for (let i = 0; i < inputArray.length; i++) {
    if (!isFieldValid(inputArray[i])) {
      inputArray[i].scrollIntoView({ block: "center", behavior: "smooth" });
      return false;
    }
  }

  return true;
};

const getInputNode = (inputId) => {
  return document.querySelector(`${inputId}`);
};

const isFieldValid = (input, message) => {
  if (input.value.trim() === "" && input.classList.contains("required")) {
    setFieldError(input, message);
    return false;
  } else {
    setFieldValid(input);
    return true;
  }
};

const setFieldError = (input) => {
  const inputWrapper = input.parentElement;

  inputWrapper.classList.add("error-field");
  input.classList.add("input-error");
};

const setFieldValid = (input) => {
  const inputWrapper = input.parentElement;
  inputWrapper.classList.remove("error-field");
  input.classList.remove("input-error");
};

//geocoding api

const setZipValues = () => {
  const zipInput = document.querySelector("#zip");
  const cityInput = document.querySelector("#city");
  const countryInput = document.querySelector("#country");
  const stateInput = document.querySelector("#state");

  zipInput.addEventListener("input", async (value) => {
    if (value.target.value.length < 5 && googleCalls < maxGoogleCalls) {
      googleCalls++;

      const payload = await getAddress();
      if (payload.data.results[0]) {
        const { country, state, city } = geocodingParse(payload.data.results[0]);
        countryInput.value = country;
        stateInput.value = state;
        cityInput.value = city;
      }
      if (payload && payload.status === "ZERO_RESULTS") {
        countryInput.value = "";
        stateInput.value = "";
        cityInput.value = "";
      }
    }
  });
};

const geocodingParse = (payload) => {
  const addresses = payload.address_components;
  const formattedResponse = {
    country: "",
    city: "",
    state: "",
  };

  addresses?.forEach((i) => {
    if (i.types[0] === "country") {
      formattedResponse.country = i.short_name;
    }
    if (i.types[0] === defineCityType(i.types[0])) {
      formattedResponse.city = i.short_name;
    }
    if (i.types[0] === "administrative_area_level_1") {
      formattedResponse.state = i.long_name;
    }
  });

  return formattedResponse;
};

const defineCityType = (area) => {
  if (area === "landmark") {
    return "landmark";
  } else if (area === "natural_feature") {
    return "natural_feature";
  } else if (area === "establishment") {
    return "establishment";
  } else if (area === "neighborhood") {
    return "neighborhood";
  } else if (area === "locality") {
    return "locality";
  }
};

const setHolderName = () => {
  const firstNameInput = document.querySelector("#firstName");
  const lastNameInput = document.querySelector("#lastName");
  const holderNameInput = document.querySelector("#holderName");

  let inpt1Val = "";
  let inpt2Val = "";

  firstNameInput.addEventListener("input", (e) => {
    inpt1Val = e.target.value;
    holderNameInput.value = `${inpt1Val} ${inpt2Val}`;
  });

  lastNameInput.addEventListener("input", (e) => {
    inpt2Val = e.target.value;
    holderNameInput.value = `${inpt1Val} ${inpt2Val}`;
  });
};
