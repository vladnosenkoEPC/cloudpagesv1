const api_url = "https://api.phoenixfinancialresearch.dev/api";
const geolocationApiKey = "AIzaSyD3TdwgAZQibgwqJGQQjRcSbqqqFgLKtwc";

const language = "en";
const country = "us";

const postSubscription = async (data) => {
  let config = {
    headers: {
      "Content-Type": "application/ld+json",
      Accept: "application/ld+json",
    },
  };

  try {
    const response = await axios.post(`${api_url}/payments/checkout`, data, config);
    const respData = response.data;
    if (respData) alert("Successfully bought the subscription");
    return respData;
  } catch (errors) {
    console.error(errors);
  }
};

const getAddress = async (zip) => {
  try {
    const response = axios.get(
      `https://maps.googleapis.com/maps/api/geocode/json?language=${language}&country=${country}&address=${zip}&sensor=true&key=${geolocationApiKey}`,
    );
    return response;
  } catch (errors) {
    console.error(errors);
  }
};
